package au.aossie.scavenger

import ammonite.ops._
import au.aossie.scavenger.parser.TPTP.CNF
import au.aossie.scavenger.parser.TPTPCNFParser
import au.aossie.scavenger.prover.{EPCR, Unsatisfiable}
import org.specs2.mutable.Specification

class IncludeDirectiveTest extends Specification {
  val input = pwd / 'examples / 'problems / 'CNF / "CNF.cnfp"
  val axiomDirectoryPath = pwd / 'examples / 'problems / "Axioms"
  val axiomDirectoryPath2 = pwd / 'examples / "Axioms"
  val includeFilePath = RelPath("examples/problems/Axioms/SET001-0.ax")
  val incorrectIncludeFilePath = RelPath("examples/problems/Axioms/AHG-0.ax") //no such file
  val actualIncludeFilePath = pwd / 'examples / 'problems / 'Axioms /  "SET001-0.ax"

  val cnf = TPTPCNFParser.parse(input,axiomDirectoryPath)
  val cnf2 = TPTPCNFParser.parse(input) //default case

  "Scavenger" should {
    "prove CNF.cnfp" in { EPCR.prove(cnf).isInstanceOf[Unsatisfiable] shouldEqual true }
    "prove CNF.cnfp" in { EPCR.prove(cnf2).isInstanceOf[Unsatisfiable] shouldEqual true }
    "find correct path" in { CNF.getFilePath(axiomDirectoryPath,includeFilePath) shouldEqual actualIncludeFilePath}
    "find correct path" in { CNF.getFilePath(axiomDirectoryPath,RelPath("SET001-0.ax")) shouldEqual actualIncludeFilePath}
    "detect incorrect path" in { CNF.getFilePath(axiomDirectoryPath2,RelPath("SET001-0.ax")) must throwA[Exception]("Failure: IncludeDirective file path incorrect")}//incorrect axiom folder
    "detect incorrect path" in { CNF.getFilePath(axiomDirectoryPath,incorrectIncludeFilePath) must throwA[Exception]("Failure: IncludeDirective file path incorrect")}//no such file
  }
}